import java.math.MathContext;
import java.math.RoundingMode;
import java.util.*;
import java.math.BigDecimal;

public class singleThreadPi implements Runnable {

    static int k;

    public static void main(String[] args) throws InterruptedException {

        Scanner in = new Scanner(System.in);
        System.out.println("Enter k: ");
        k = in.nextInt();

        long initTime = System.currentTimeMillis();
        singleThreadPi pi = new singleThreadPi();
        Thread thread = new Thread(pi);
        thread.start();
        thread.join();
        long endTime = System.currentTimeMillis();
        System.out.println("in main() " + (endTime - initTime));

    }

    @Override
    public void run() {

        MathContext piPrecision = new MathContext(500);
        MathContext numOfDecimalPlaces = new MathContext(100, RoundingMode.HALF_UP);

        BigDecimal Pi = new BigDecimal("0.0");

        BigDecimal one = BigDecimal.ONE;
        BigDecimal two = new BigDecimal("2.0");
        BigDecimal four = new BigDecimal("4.0");
        BigDecimal five = new BigDecimal("5.0");
        BigDecimal six = new BigDecimal("6.0");
        BigDecimal eight = new BigDecimal("8.0");
        BigDecimal sixteen = new BigDecimal("16.0");



        for(int i = 0; i < k; i++) {

            BigDecimal sixteenToI = sixteen.pow(i);
            BigDecimal oneOverSixteenToI = one.divide(sixteenToI, numOfDecimalPlaces);

            BigDecimal eightTimesI = eight.multiply(new BigDecimal(i));

            BigDecimal frac1 = four.divide(eightTimesI.add(one), numOfDecimalPlaces);
            BigDecimal frac2 = two.divide(eightTimesI.add(four), numOfDecimalPlaces);
            BigDecimal frac3 = one.divide(eightTimesI.add(five), numOfDecimalPlaces);
            BigDecimal frac4 = one.divide(eightTimesI.add(six), numOfDecimalPlaces);

            Pi = Pi.add(oneOverSixteenToI.multiply(
                    frac1.subtract(frac2)
                            .subtract(frac3)
                            .subtract(frac4)), piPrecision);

            //System.out.println("oneOverSixteenToI= "+oneOverSixteenToI);
            /*System.out.println("Pi= "+Pi);*/
            /*System.out.println("1/16^i = "+1 / Math.pow(16, i));*/
            /*System.out.println("f1= "+frac1);
            System.out.println("f2= "+frac2);
            System.out.println("f3= "+frac3);
            System.out.println("f4= "+frac4);*/
        }
        System.out.println("Pi= "+Pi);
    }
}
// pi:
// 3.1415926535 8979323846 2643383279 5028841971 6939937510 5820974944 5923078164 0628620899 8628034825 3421170679 --org
// 3.1415926535 8979323846 2643383279 5028841971 6939937510 5820974944 5923078164 0628620899 8628034825 3421170679 (BEST so far)
// 3.1415926535 8979323846 2643383279 5028841971 6939937510 5820974944 5922466544 4819062093 1912515948 2533123956
// 3.1415926535 8979323846 2643383279 5028841971 6939937510 5820974944 5922466544 4819062093 1912515948 2533123956
// 3.1415926535 8979323846 2643383279 1185455590 6699942443 0631813644 8549950033 0113117534 3630856438 9241839931
// 3.1415926535 8979323846 2643382784 0915142466 2361132933 4708720045 2570995531 8322528309 7675898688 7853094680
// 3.1415926535 8979323846 2643382784 0915142466 2361132933 4708720045 2570995531 8322528309 7675898688 7853094679

//double sixteenPow = 1 / Math.pow(16, i);
//double frac1 =  4.0 / ((8 * i) + 1);
//double frac2 = 2.0 / ((8 * i) + 4);
//double frac3 = 1.0 / ((8 * i) + 5);
//double frac4 = 1.0 / ((8 * i) + 6);
