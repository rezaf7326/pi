import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.locks.ReentrantLock;

public class multiThreadPi implements Runnable {

    static ReentrantLock reLockI = new ReentrantLock();
    static ReentrantLock reentrantLock = new ReentrantLock();

    static volatile ArrayList<BigDecimal> piComponents = new ArrayList<>();

    MathContext piPrecision = new MathContext(500);
    MathContext numOfDecimalPlaces = new MathContext(100, RoundingMode.HALF_UP);

    static BigDecimal Pi = new BigDecimal("0.0");
    BigDecimal one = BigDecimal.ONE;
    BigDecimal two = new BigDecimal("2.0");
    BigDecimal four = new BigDecimal("4.0");
    BigDecimal five = new BigDecimal("5.0");
    BigDecimal six = new BigDecimal("6.0");
    BigDecimal eight = new BigDecimal("8.0");
    BigDecimal sixteen = new BigDecimal("16.0");


    static int i = 0;
    static volatile int iCallCounter = -1;


    public static void main(String[] args) throws InterruptedException {

        Scanner in = new Scanner(System.in);
        System.out.println("Enter k(in multiThread): ");
        int k = in.nextInt();
        long initTime = System.currentTimeMillis();
        multiThreadPi MULTI_PI = new multiThreadPi();
        ArrayList<Thread> threads = new ArrayList<>();

        for (; i < k; i++) {
            Thread thread = new Thread(MULTI_PI);
            threads.add(thread);
            thread.start();
        }

        for (Thread thread:threads)
            thread.join();

        for(BigDecimal piComponent:piComponents)
            Pi = Pi.add(piComponent);

        System.out.println(Pi);
        long endTime = System.currentTimeMillis();
        System.out.println(endTime - initTime);
    }




    @Override
    public void run() {
        //System.out.println(Thread.currentThread().getId() + " " + getICounter());

        int iCount = getICounter();
        BigDecimal sixteenToI = sixteen.pow(iCount);
        BigDecimal oneOverSixteenToI = one.divide(sixteenToI, numOfDecimalPlaces);
        BigDecimal eightTimesI = eight.multiply(new BigDecimal(iCount));
        BigDecimal frac1 = four.divide(eightTimesI.add(one), numOfDecimalPlaces);
        BigDecimal frac2 = two.divide(eightTimesI.add(four), numOfDecimalPlaces);
        BigDecimal frac3 = one.divide(eightTimesI.add(five), numOfDecimalPlaces);
        BigDecimal frac4 = one.divide(eightTimesI.add(six), numOfDecimalPlaces);

        reentrantLock.lock();
        piComponents.add(oneOverSixteenToI.multiply(
                            frac1.subtract(frac2)
                                 .subtract(frac3)
                                 .subtract(frac4), piPrecision));
        reentrantLock.unlock();
    }




    private static int getICounter () {

        reLockI.lock();
        iCallCounter++;
        reLockI.unlock();
        return iCallCounter;
    }
}
